#!/bin/sh
mkdir -p /tmp/tclx-cvs
rsync -ai a.cvs.sourceforge.net::cvsroot/tclx/ /tmp/tclx-cvs

mkdir -p /tmp/tclx-git
cd /tmp/tclx-git

cvs2git --blobfile=blob.dat --dumpfile=dump.dat \
    --username=cvs --default-eol=LF \
    --encoding=utf8 --encoding=latin1 --fallback-encoding=ascii \
    --symbol-transform='TCLX_([0-9])_([0-9])_([0-9]):v\1.\2.\3' \
    --symbol-transform='TCLX_([0-9])_([0-9])_([0-9])B([0-9])(.*):v\1.\2.\3-beta\4' \
    --symbol-transform='TclX([0-9])_([0-9])_([0-9]):v\1.\2.\3' \
    --symbol-transform='TclX([0-9])_([0-9])a:v\1.\2.1' \
    --symbol-transform='TclX([0-9])_([0-9])b:v\1.\2.2' \
    --symbol-transform='TclX([0-9])_([0-9])c:v\1.\2.3' \
    /tmp/tclx-cvs/tclx

git init
cat blob.dat dump.dat | git fast-import
git checkout

# remove useless old cruft

git tag -d $(git tag | grep -v '^v')
git branch -D $(git branch | grep -v master)
